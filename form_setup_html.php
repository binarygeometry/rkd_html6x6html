<?php defined('C5_EXECUTE') or die("Access Denied."); ?>  
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="layout">
                        <option value="right" <?php if($layout == "right") echo "selected"; ?>>Right</option>
                        <option value="left"  <?php if($layout == "left")  echo "selected"; ?>>Left</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <input type="checkbox" name="border" <?php if($border == "on") echo "checked"; ?> >
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="row">
    <?php if($layout == "right"):?>
    <div class="col-md-6 col-md-push-6">
    <?php else: ?>
    <div class="col-md-6">
    <?php endif;?>
        <div class="form-group">
            <input name="title"   placeholder="title"   value="<?php echo h($title)?>">
        </div>
        <div class="form-group">
            <textarea name="intro" rows="10"><?php echo h($intro)?></textarea>
        </div>
    </div>
    <?php if($layout == "right"):?>
    <div class="col-md-6 col-md-pull-6">
    <?php else: ?>
    <div class="col-md-6">
    <?php endif;?>
        <textarea style="display: none" id="ccm-block-html-value-textarea" name="content"></textarea>
        <div id="ccm-block-html-value"><?php echo h($content)?></div>
    </div>
</div>

<style type="text/css">
    #ccm-block-html-value {
        width: 100%;
        border: 1px solid #eee;
        height: 490px;
    }
</style>

<script type="text/javascript">
    $(function() {
        var editor = ace.edit("ccm-block-html-value");
        console.log('do', editor)
        editor.setTheme("ace/theme/eclipse");
        editor.getSession().setMode("ace/mode/html");
        refreshTextarea(editor.getValue());
        editor.getSession().on('change', function() {
            refreshTextarea(editor.getValue());
        });
    });

    function refreshTextarea(contents) {
        console.log('co', contents)
      $('#ccm-block-html-value-textarea').val(contents);
    }
</script>
