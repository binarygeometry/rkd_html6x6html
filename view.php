<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="container">
<?php if($border == "on"):?><div class="theme-block-border"><?php endif;?>
<div class="row">
    <?php if($layout == "right"):?>
    <div class="col-md-6 col-md-push-6">
    <?php else: ?>
    <div class="col-md-6">
    <?php endif;?>
      <div id="HTMLBlock<?php echo intval($bID)?>" class="HTMLBlock">
        <?php echo $content; ?>
      </div>
    </div>
    <?php if($layout == "right"):?>
    <div class="col-md-6 col-md-pull-6">
    <?php else: ?>
    <div class="col-md-6">
    <?php endif;?>
        <h3 class="festival-title"><?php echo h($title);?></h3>
        <p class="festival-intro"><?php echo h($intro);?></p>
    </div>
</div>
<?php if($border == "on"):?></div><?php endif;?>
</div>